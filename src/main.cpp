#include <Arduino.h>
#include <AccelStepper.h>
#include <LowPower.h>
#include <JC_Button.h>

#define BUTTON_PIN 2

// can either do a few every short interval,
// or a bunch every long interval
#define WIND_INTERVAL_SEC 1800UL // 30-min
#define WIND_TRAVEL_TOTAL_MS 1000UL
#define WIND_COUNT 10

// 28-BYJ48 + ULN2003
// Step angle: 5.625 x 1/64

// (360 / 5.625) * 64 = 4096
const long STEPPER_FULL_ROTATION = 4096;
AccelStepper stepper(AccelStepper::HALF4WIRE, 6, 8, 7, 9);

Button btn(BUTTON_PIN);

uint16_t state = 0;
uint16_t iteration = 0;
volatile bool wakey = false;

void interruptHandler()
{
    wakey = true;
}

void setup()
{
     btn.begin();

    // max speed is dependent on the chip...
    // 2000 is ~6 seconds for a full rotation
    stepper.setMaxSpeed(2000); // steps per second
    stepper.setAcceleration(400.0); // steps per second
}

void loop()
{
    stepper.run();
    btn.read();

    switch (state) {
        case 0:
            stepper.enableOutputs();
            stepper.moveTo(STEPPER_FULL_ROTATION / 2);
            iteration = 0;
            state = 1;
            break;
        case 1:
            if (btn.wasPressed()) {
                stepper.moveTo(0);
                state = 4;
            }
            else if (!stepper.isRunning()) {
                stepper.moveTo(-STEPPER_FULL_ROTATION / 2);
                state = 2;
            }
            break;
        case 2:
            if (btn.wasPressed()) {
                stepper.moveTo(0);
                state = 4;
            }
            else if (!stepper.isRunning()) {
                stepper.moveTo(STEPPER_FULL_ROTATION / 2);
                state = 3;
            }
            break;
        case 3:
            if (btn.wasPressed()) {
                stepper.moveTo(0);
                state = 4;
            }
            else if (!stepper.isRunning()) {
                if (++iteration >= WIND_COUNT) {
                    stepper.moveTo(0);
                    state = 4;
                }
                else {
                    state = 1;
                }
            }
            break;
        case 4:
            if (!stepper.isRunning()) {
                stepper.disableOutputs();
                wakey = false;
                attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), interruptHandler, LOW);
                iteration = 0;
                state = 5;
            }
            break;
        case 5:
            if (++iteration >= WIND_INTERVAL_SEC || wakey) {
                detachInterrupt(digitalPinToInterrupt(BUTTON_PIN));
                wakey = false;
                state = 0;
            }
            else {
                LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
            }
            break;
    }
}

// POWER switch
// (470uF or greater) between 5V and ground of servo pins
// at intervals, slowly sweep from 0 - 180, then back to 90

// optional:
// button + led that is pressed to turn off activity for some period of time
// oled screen to show status?
// 3-button (4?) control to control parameters?
// continuous wind for some interval?
