# Watch Winder

Watch Winder using an Arduino Mini.

# Parts

- arduino mini
- push button (https://www.amazon.com/gp/product/B075VBWFM6)
- power switch (https://www.amazon.com/gp/product/B07B2S5QYN)
- 28BYJ-48 stepper (https://www.amazon.com/gp/product/B01CP18J4A)
- 2.1mm DC power jack (https://www.amazon.com/gp/product/B01N8VV78D)
- 4x 4mm cube magnets

# Notes

- See SVG files for the lasercut box
- FTDI programmer, since the Arduino Mini has no USB

# Box Notes

- barely enough clearance for motor; may want to increase
- not a lot of space for wire routing; may want to increase width
- buttons had to be put in front; forgot about the back panel;
  may want to adjust placement
- face plate hole should be larger; give more clearance
